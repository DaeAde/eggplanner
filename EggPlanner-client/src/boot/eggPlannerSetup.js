// import Vue from 'vue'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyCI53zZwF2GjN37WiFIYncX0B3C9kNiCAc',
  authDomain: 'egg-planner-16a26.firebaseapp.com',
  databaseURL: 'https://egg-planner-16a26.firebaseio.com',
  projectId: 'egg-planner-16a26',
  storageBucket: 'egg-planner-16a26.appspot.com',
  messagingSenderId: '311374752123',
  appId: '1:311374752123:web:7413b5be0b04ad3bb375de',
  measurementId: 'G-XTJKGDHEDN'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}
