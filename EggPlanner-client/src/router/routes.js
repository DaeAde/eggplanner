const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/Login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') }
    ]
  },
  {
    path: '/Administration',
    component: () => import('layouts/EggPlannerLayout.vue'),
    meta: { auth: true },
    children: [
      { path: '/Schedules', meta: { auth: true }, component: () => import('pages/EggPlanner/SchedulesIndex.vue') },
      { path: '/Users', meta: { auth: true }, component: () => import('pages/EggPlanner/UsersIndex.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
