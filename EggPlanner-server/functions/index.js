const express = require('express');
const bodyParser = require('body-parser');
const { response, request } = require('express');
const app = express();
const models = require('./models.js')

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

var admin = require("firebase-admin");

var serviceAccount = require("./egg-planner-16a26-firebase-adminsdk-xf7hw-b8760aa71e.json");
const { query } = require('express');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://egg-planner-16a26.firebaseio.com"
});

const db = admin.firestore();

//get, create, update, delete functions for schedules collection
// get for /schedules
app.get("/schedules", (req, res) => {
    let id = (
        typeof req.query.id !== "undefined"
            ? req.query.id
            : null
    )
    let subCollection = (
        typeof req.query.subCollection !== "undefined"
            ? req.query.subCollection
            : null
    )
    let order = null
    let where = null
    if ( id === null) {
        order = models.getOrder(req.query)
        where = models.getWhere(req.query)
    }
        models.get(db, "schedules", id, order, subCollection)
            .then(response => {
                return res.send(response)
            })
            .catch(error => {
                return res.send(error)
            })
})

//create for /schedules
app.post("/schedules", (req, res) => {
    if (typeof req.query.id === 'undefined') {
        if (Object.keys(req.body).length) {
            db.collection('schedules').doc().set(req.body)
                .then(function () {
                    return res.send(
                        "Document successfully written - created!"
                    )
                })
                .catch(function (error) {
                    return res.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return res.send(
                "No post data for new document. " +
                "A new document is not created!"
            )
        }
       } else {
            if (typeof req.query.subCollection === 'undefined') {
                if (Object.keys(req.body).length) {
                    db.collection('schedules').doc().set(req.body)
                        .then(function () {
                            return res.send(
                                "Document successfully written - created!"
                            )
                        })
                        .catch(function (error) {
                            return res.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return res.send(
                        "No post data for new document. " +
                        "A new document is not created!"
                    )
                }
            } else {
                if (Object.keys(req.body).length) {
                    db.collection('schedules').doc(req.query.id).collection(req.query.subCollection).doc().set(req.body)
                        .then(function () {
                            return res.send(
                                "Document successfully written - created!"
                            )
                        })
                        .catch(function (error) {
                            return res.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return res.send(
                        "No post data for new document. " +
                        "A new document is not created!"
                    )
                }
            }
}})


//update for /schedules

app.put('/schedules', (request, response) => {
    if (typeof request.query.id === 'undefined') {
        if (Object.keys(request.body).length) {
            if (typeof request.query.id !== 'undefined') {
                db.collection('schedules')
                    .doc(request.query.id)
                    .update(request.body)
                    .then(function () {
                        return response.send(
                            "Document successfully written - " + "updated!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error writing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not updated!"
                )
            }
        } else {
            return response.send(
                "No post data for new document. " +
                "A document is not updated!"
            )
        }
   } else {
        if (typeof request.query.subCollection === 'undefined') {
            if (Object.keys(request.body).length) {
                if (typeof request.query.id !== 'undefined') {
                    db.collection('schedules')
                        .doc(request.query.id)
                        .update(request.body)
                        .then(function () {
                            return response.send(
                                "Document successfully written - " + "updated!"
                            )
                        })
                        .catch(function (error) {
                            return response.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return response.send(
                        "A parameter id is not set. " +
                        "A document is not updated!"
                    )
                }
            } else {
                return response.send(
                    "No post data for new document. " +
                    "A document is not updated!"
                )
            }
        } else {
            if (Object.keys(request.body).length) {
                if (typeof request.query.id !== 'undefined' && typeof request.query.subCollection !== 'undefined') {
                    db.collection('schedules')
                        .doc(request.query.id)
                        .collection(request.query.subCollection)
                        .doc(request.query.subCollectionID)
                        .update(request.body)
                        .then(function () {
                            return response.send(
                                "Document successfully written - " + "updated!"
                            )
                        })
                        .catch(function (error) {
                            return response.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return response.send(
                        "A parameter id is not set. " +
                        "A document is not updated!"
                    )
                }
            } else {
                return response.send(
                    "No post data for new document. " +
                    "A document is not updated!"
                )
            }
   }
}})  

//delete for /schedules
app.delete('/schedules', async (request, response) => {
    if (typeof request.query.id === 'undefined') {
        if (typeof request.query.id !== 'undefined') {
            db.collection('schedules').doc(request.query.id).delete()
                .then(function () {
                    return response.send(
                        "Document successfully deleted!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Error removing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not deleted!"
            )
        }
    } else {
        if (typeof request.query.subCollection === 'undefined') {
            if (typeof request.query.id !== 'undefined') {
                db.collection('schedules').doc(request.query.id).delete()
                    .then(function () {
                        return response.send(
                            "Document successfully deleted!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error removing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not deleted!"
                )
            }
        } else {
            if (typeof request.query.id !== 'undefined') {
                db.collection('schedules').doc(request.query.id).collection(request.query.subCollection).doc(request.query.subCollectionID).delete()
                    .then(function () {
                        return response.send(
                            "Document successfully deleted!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error removing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not deleted!"
                )
            }
        }
    }
})

//get, create, update, delete functions for users collection
// get for /users
app.get("/users", (req, res) => {
    let id = (
        typeof req.query.id !== "undefined"
            ? req.query.id
            : null
    )
    let subCollection = (
        typeof req.query.subCollection !== "undefined"
            ? req.query.subCollection
            : null
    )
    let order = null
    let where = null
    if ( id === null) {
        order = models.getOrder(req.query)
        where = models.getWhere(req.query)
    }
        models.get(db, "users", id, order, subCollection)
            .then(response => {
                return res.send(response)
            })
            .catch(error => {
                return res.send(error)
            })
})

//create for /users
app.post("/users", (req, res) => {
    if (typeof req.query.id === 'undefined') {
        if (Object.keys(req.body).length) {
            db.collection('users').doc().set(req.body)
                .then(function () {
                    return res.send(
                        "Document successfully written - created!"
                    )
                })
                .catch(function (error) {
                    return res.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return res.send(
                "No post data for new document. " +
                "A new document is not created!"
            )
        }
       } else {
            if (typeof req.query.subCollection === 'undefined') {
                if (Object.keys(req.body).length) {
                    db.collection('users').doc().set(req.body)
                        .then(function () {
                            return res.send(
                                "Document successfully written - created!"
                            )
                        })
                        .catch(function (error) {
                            return res.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return res.send(
                        "No post data for new document. " +
                        "A new document is not created!"
                    )
                }
            } else {
                if (Object.keys(req.body).length) {
                    db.collection('users').doc(req.query.id).collection(req.query.subCollection).doc().set(req.body)
                        .then(function () {
                            return res.send(
                                "Document successfully written - created!"
                            )
                        })
                        .catch(function (error) {
                            return res.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return res.send(
                        "No post data for new document. " +
                        "A new document is not created!"
                    )
                }
            }
}})


//update for /users

app.put('/users', (request, response) => {
    if (typeof request.query.id === 'undefined') {
        if (Object.keys(request.body).length) {
            if (typeof request.query.id !== 'undefined') {
                db.collection('users')
                    .doc(request.query.id)
                    .update(request.body)
                    .then(function () {
                        return response.send(
                            "Document successfully written - " + "updated!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error writing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not updated!"
                )
            }
        } else {
            return response.send(
                "No post data for new document. " +
                "A document is not updated!"
            )
        }
   } else {
        if (typeof request.query.subCollection === 'undefined') {
            if (Object.keys(request.body).length) {
                if (typeof request.query.id !== 'undefined') {
                    db.collection('users')
                        .doc(request.query.id)
                        .update(request.body)
                        .then(function () {
                            return response.send(
                                "Document successfully written - " + "updated!"
                            )
                        })
                        .catch(function (error) {
                            return response.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return response.send(
                        "A parameter id is not set. " +
                        "A document is not updated!"
                    )
                }
            } else {
                return response.send(
                    "No post data for new document. " +
                    "A document is not updated!"
                )
            }
        } else {
            if (Object.keys(request.body).length) {
                if (typeof request.query.id !== 'undefined' && typeof request.query.subCollection !== 'undefined') {
                    db.collection('users')
                        .doc(request.query.id)
                        .collection(request.query.subCollection)
                        .doc(request.query.subCollectionID)
                        .update(request.body)
                        .then(function () {
                            return response.send(
                                "Document successfully written - " + "updated!"
                            )
                        })
                        .catch(function (error) {
                            return response.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return response.send(
                        "A parameter id is not set. " +
                        "A document is not updated!"
                    )
                }
            } else {
                return response.send(
                    "No post data for new document. " +
                    "A document is not updated!"
                )
            }
   }
}})  

//delete for /users
app.delete('/users', async (request, response) => {
    if (typeof request.query.id === 'undefined') {
        if (typeof request.query.id !== 'undefined') {
            db.collection('users').doc(request.query.id).delete()
                .then(function () {
                    return response.send(
                        "Document successfully deleted!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Error removing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not deleted!"
            )
        }
    } else {
        if (typeof request.query.subCollection === 'undefined') {
            if (typeof request.query.id !== 'undefined') {
                db.collection('users').doc(request.query.id).delete()
                    .then(function () {
                        return response.send(
                            "Document successfully deleted!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error removing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not deleted!"
                )
            }
        } else {
            if (typeof request.query.id !== 'undefined') {
                db.collection('users').doc(request.query.id).collection(request.query.subCollection).doc(request.query.subCollectionID).delete()
                    .then(function () {
                        return response.send(
                            "Document successfully deleted!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error removing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not deleted!"
                )
            }
        }
    }
})


//get, create, update, delete functions for uses collection
// get for /uses
app.get("/uses", (req, res) => {
    let id = (
        typeof req.query.id !== "undefined"
            ? req.query.id
            : null
    )
    let subCollection = (
        typeof req.query.subCollection !== "undefined"
            ? req.query.subCollection
            : null
    )
    let order = null
    let where = null
    if ( id === null) {
        order = models.getOrder(req.query)
        where = models.getWhere(req.query)
    }
        models.get(db, "uses", id, order, subCollection)
            .then(response => {
                return res.send(response)
            })
            .catch(error => {
                return res.send(error)
            })
})

//create for /uses
app.post("/uses", (req, res) => {
    if (typeof req.query.id === 'undefined') {
        if (Object.keys(req.body).length) {
            db.collection('uses').doc().set(req.body)
                .then(function () {
                    return res.send(
                        "Document successfully written - created!"
                    )
                })
                .catch(function (error) {
                    return res.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return res.send(
                "No post data for new document. " +
                "A new document is not created!"
            )
        }
       } else {
            if (typeof req.query.subCollection === 'undefined') {
                if (Object.keys(req.body).length) {
                    db.collection('uses').doc().set(req.body)
                        .then(function () {
                            return res.send(
                                "Document successfully written - created!"
                            )
                        })
                        .catch(function (error) {
                            return res.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return res.send(
                        "No post data for new document. " +
                        "A new document is not created!"
                    )
                }
            } else {
                if (Object.keys(req.body).length) {
                    db.collection('usess').doc(req.query.id).collection(req.query.subCollection).doc().set(req.body)
                        .then(function () {
                            return res.send(
                                "Document successfully written - created!"
                            )
                        })
                        .catch(function (error) {
                            return res.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return res.send(
                        "No post data for new document. " +
                        "A new document is not created!"
                    )
                }
            }
}})


//update for /uses

app.put('/uses', (request, response) => {
    if (typeof request.query.id === 'undefined') {
        if (Object.keys(request.body).length) {
            if (typeof request.query.id !== 'undefined') {
                db.collection('uses')
                    .doc(request.query.id)
                    .update(request.body)
                    .then(function () {
                        return response.send(
                            "Document successfully written - " + "updated!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error writing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not updated!"
                )
            }
        } else {
            return response.send(
                "No post data for new document. " +
                "A document is not updated!"
            )
        }
   } else {
        if (typeof request.query.subCollection === 'undefined') {
            if (Object.keys(request.body).length) {
                if (typeof request.query.id !== 'undefined') {
                    db.collection('uses')
                        .doc(request.query.id)
                        .update(request.body)
                        .then(function () {
                            return response.send(
                                "Document successfully written - " + "updated!"
                            )
                        })
                        .catch(function (error) {
                            return response.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return response.send(
                        "A parameter id is not set. " +
                        "A document is not updated!"
                    )
                }
            } else {
                return response.send(
                    "No post data for new document. " +
                    "A document is not updated!"
                )
            }
        } else {
            if (Object.keys(request.body).length) {
                if (typeof request.query.id !== 'undefined' && typeof request.query.subCollection !== 'undefined') {
                    db.collection('uses')
                        .doc(request.query.id)
                        .collection(request.query.subCollection)
                        .doc(request.query.subCollectionID)
                        .update(request.body)
                        .then(function () {
                            return response.send(
                                "Document successfully written - " + "updated!"
                            )
                        })
                        .catch(function (error) {
                            return response.send(
                                "Error writing document: " + error
                            )
                        })
                } else {
                    return response.send(
                        "A parameter id is not set. " +
                        "A document is not updated!"
                    )
                }
            } else {
                return response.send(
                    "No post data for new document. " +
                    "A document is not updated!"
                )
            }
   }
}})  

//delete for /uses
app.delete('/uses', async (request, response) => {
    if (typeof request.query.id === 'undefined') {
        if (typeof request.query.id !== 'undefined') {
            db.collection('uses').doc(request.query.id).delete()
                .then(function () {
                    return response.send(
                        "Document successfully deleted!"
                    )
                })
                .catch(function (error) {
                    return response.send(
                        "Error removing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not deleted!"
            )
        }
    } else {
        if (typeof request.query.subCollection === 'undefined') {
            if (typeof request.query.id !== 'undefined') {
                db.collection('uses').doc(request.query.id).delete()
                    .then(function () {
                        return response.send(
                            "Document successfully deleted!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error removing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not deleted!"
                )
            }
        } else {
            if (typeof request.query.id !== 'undefined') {
                db.collection('uses').doc(request.query.id).collection(request.query.subCollection).doc(request.query.subCollectionID).delete()
                    .then(function () {
                        return response.send(
                            "Document successfully deleted!"
                        )
                    })
                    .catch(function (error) {
                        return response.send(
                            "Error removing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "A parameter id is not set. " +
                    "A document is not deleted!"
                )
            }
        }
    }
})

/*
app.get('/getpuser', (request, response) => {
    let res = [];
    db.collection('plannerUser')
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                let document = {
                    id: doc.id, 
                    data: doc.data()
                }
                res.push(document)
            })
            return response.send(res)
        })
        .catch ((error) => {
            return response.send("Error getting documents: ", error);
        })
})

app.post('/addpuser' , (request, response) => {
    const data =request.body;
    console.log(data.puser);
    return response.send('POST metoda -> Add '+data.puser);
})

app.put('/changepuser', (request, response) => {
    const data = request.body;
    console.log(data.puser);
    return response.send('PUT metoda -> Change '+data.puser);
})

app.delete('/delpuser', (request, response) => {
    const data = request.body;
    console.log('Delete '+data.puser);
    return response.send('Delete '+data.puser);
})
*/
app.listen(3000, () => {
    console.log("Servers runing on port 3000");
});